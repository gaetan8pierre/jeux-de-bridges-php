<?php
/**
 * Created by PhpStorm.
 * User: Bahia
 * Date: 10/12/2018
 * Time: 19:01
 */
class VueJeu
{
    function afficheGrille($villesLiens) {
        echo "<table>";
        $i = 0;
        foreach ($villesLiens as $ligne) {
            echo "<tr>";
            $j = 0;
            foreach ($ligne as $case) {
                if (is_int($case)){
                    echo "<td><a href='?i=$i&j=$j'>$case</a></td>";
                }
                if($case=="v"){
                    echo "<td></td>";
                } else {
                    echo "<td>$case</td>";
                }
                $j++;
            }
            echo "</tr>";
            $i++;
        }
    }
}
