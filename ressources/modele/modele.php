<?php

    //Classe permettant de gérer globalement les Exceptions
    Class LesExcept extends Exception{
        private $chaine;
        public function __construct($chaine){
            $this->chaine=$chaine;
        }

        public function afficher(){
            return $this->chaine;
        }

    }

    //Pour les eceptions liées à la connexion d'un joueur
    class ConnexionExcept extends LesExcept{
    }

    //Pour les exceptions liées aux stats de fin de jeu
    class StatEcept extends LesExcept{
    }

    //Pour les exception d'accès à une table
    class TableAccesExcept extends LesExcept{
    }

    //Classe de connection à la base de donée
    class Modele
    {
        private $connexion;

        public function __construct()
        {
            try {
                $chaine = "mysql:host=" . HOST . ";dbname=" . BD;
                $this->connexion = new PDO($chaine, LOGIN, PASSWORD);
                $this->connexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                $exception = new ConnexionExcept("La tentative de connexion à échouée");
                throw $exception;
            }

        }

        //classe vérifiant l'existance de quelque chose prise dans la correction de Madame Jacquin
        public function exists($pseudo)
        {
            try {
                $statement = $this->connexion->prepare("select id from pseudonyme where pseudo=?;");
                $statement->bindParam(1, $pseudoParam);
                $pseudoParam = $pseudo;
                $statement->execute();
                $result = $statement->fetch(PDO::FETCH_ASSOC);

                if ($result["id"] != NUll) {
                    return true;
                } else {
                    return false;
                }
            } catch (PDOException $e) {
                $this->deconnexion();
                throw new TableAccesException("problème avec la table joueurs");
            }
        }

        public function connexionValide($pseudo, $motdepasse){
            try{
                $statement = $this->connexion->prepare("select * from joueurs where pseudo=?");
                $statement->bindParam(1, $pseudo);
                $statement->execute();
                $result = $statement->fetch(PDO::FETCH_ASSOC);
                if($result["pseudo"]==$pseudo and crypt($motdepasse, $result["motDePasse"])==$result["motDePasse"]){
                    return true;
                }else{
                    return false;
                }
            }catch (PDOException $e) {
                throw new TableAccesExcept("Problème avec l'accès au table");
            }
        }
    }




