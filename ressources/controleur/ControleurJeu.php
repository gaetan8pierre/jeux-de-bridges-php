<?php
/**
 * Created by PhpStorm.
 * User: Bahia
 * Date: 10/12/2018
 * Time: 20:19
 */
require_once PATH_VUE."VueJeu.php";
require_once PATH_MODELE."Villes.php";

class ControleurJeu
{
    private $vueJeu;
    private $modele;

    function __construct(){
        $this->modele = new Villes();
        $this->vueJeu = new VueJeu();
    }

    function afficheJeu(){
        
    }

    function villeClic(){
        $this->modele->getVille();
    }
}
