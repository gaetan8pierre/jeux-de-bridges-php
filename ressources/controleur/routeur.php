<?php
require_once 'ControleurAuthent.php';
require_once 'ControleurJeu.php';



class Routeur
{

    public function __construct()
    {
        $this->ctrlAuthent = new ControleurAuthent();
        $this->ctrlJeu = new ControleurJeu();
    }


    //méthode essayant de se connnecter (prise dans la correction de Madame Jacquin)
    public function routerRequete()
    {
        if (isset($_POST["pseudo"]) and isset($_POST['motdepasse'])) {
            $_SESSION["pseudo"] = $_POST["pseudo"];
            $_SESSION["motdepasse"] = $_POST["motdepasse"];
            $this->ctrlAuthent->testConnexion($_SESSION["pseudo"], $_SESSION["motdepasse"]);
        } elseif (isset($_POST["pseudoInscription"]) and isset($_POST['motdepasseInscription'])) {
            $_SESSION["pseudo"] = $_POST["pseudoInscription"];
            $_SESSION["motdepasse"] = $_POST["motdepasseInscription"];
            $this->ctrlAuthent->inscription($_SESSION["pseudo"], $_SESSION["motdepasse"]);
        }
        //if ($_SESSION["jouable"]==true){
          //  $this->ctrlJeu->afficheJeu();
        //}
        else{
            $this->ctrlAuthent->accueil();
        }




    }

}
?>
